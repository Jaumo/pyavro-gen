#!/usr/bin/env bash

#set -e

echo "Running unit tests"
python -m unittest discover -p "*test*".py
ls -l
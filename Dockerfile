FROM python:3.11

RUN apt-get update \
    && apt-get install git wget

WORKDIR /app

ADD requirements.txt /app

RUN pip3 install --upgrade pip setuptools \
    && pip3 install -r requirements.txt

ADD . /app

ENV NAME avrogen

RUN ./run_code_checks.sh
